python-json-pointer (2.4-3+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Sun, 09 Mar 2025 12:41:49 +0000

python-json-pointer (2.4-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090517).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 10:31:23 +0100

python-json-pointer (2.4-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Apr 2024 16:15:50 +0200

python-json-pointer (2.4-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Feb 2024 13:39:15 +0100

python-json-pointer (2.3-3) unstable; urgency=medium

  * Cleans better (Closes: #1046832).

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Aug 2023 10:48:20 +0200

python-json-pointer (2.3-2+apertis1) apertis; urgency=medium

  * python-jsonschema which is in development added a new
    dependency python3-json-pointer in bookworm. So move
    python3-json-pointer to development.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Thu, 08 Jun 2023 11:27:21 +0530

python-json-pointer (2.3-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 23:11:06 +0000

python-json-pointer (2.3-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 11:59:14 +0200

python-json-pointer (2.3-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Aug 2022 15:10:56 +0200

python-json-pointer (2.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Mar 2022 11:49:06 +0100

python-json-pointer (2.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Feb 2022 17:28:29 +0100

python-json-pointer (2.1-3) unstable; urgency=medium

  * Add Multi-Arch: foreign to the binary packages (Closes: #983957).

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Dec 2021 18:09:58 +0100

python-json-pointer (2.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 15:27:46 +0200

python-json-pointer (2.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 25 Aug 2021 09:28:37 +0200

python-json-pointer (2.0-2apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 06:41:56 +0000

python-json-pointer (2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 11:01:02 +0200

python-json-pointer (2.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Run wrap-and-sort -bastk.

  [ Michal Arbet ]
  * New upstream version
  * d/copyright: Add me to copyright
  * d/control:
    - Add me to uploaders
    - Bump standards to 4.5.0
    - Bump debhelper-compat to 11

 -- Michal Arbet <michal.arbet@ultimum.io>  Tue, 05 May 2020 10:37:52 +0200

python-json-pointer (1.10-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * Use debhelper-compat instead of debian/compat.
  * d/control: Set Vcs-* to salsa.debian.org

  [ Daniel Baumann ]
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * Removed Python 2 support (Closes: #937859).

 -- Thomas Goirand <zigo@debian.org>  Sun, 08 Sep 2019 14:04:49 +0200

python-json-pointer (1.10-1co1) apertis; urgency=medium

  [ Apertis package maintainers ]
  * Import Upstream version 1.10

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to sdk

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 01:45:38 +0000

python-json-pointer (1.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Fixed VCS URLs (https).
  * Added simple Debian tests.
  * Standards-Version bumped to 3.9.8 (no changes needed)
  * d/copyright
    - Fixed order
    - Added myself to Debian part
  * Added -doc package with sphinx documentation
  * Removed redundant d/docs
  * Don't run autotests with nocheck option enabled

 -- Ondřej Nový <novy@ondrej.org>  Sat, 30 Apr 2016 23:41:24 +0200

python-json-pointer (1.9-3) unstable; urgency=medium

  * override_dh_python3 to fix Py3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Oct 2015 22:38:57 +0000

python-json-pointer (1.9-2) unstable; urgency=medium

  * Uploading to unstable.
  * Standards-Version is now 3.9.6.
  * Added dh-python in build-depends, removed versions for python-all and
    python3-all.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 09:55:07 +0000

python-json-pointer (1.9-1) experimental; urgency=medium

  * New upstream release.
  * Removed applied upstream utf8 in setup.py patch.
  * Added management of /usr/bin/jsoinpointer using update-alternatives, so
    that Python 2 and 3 versions don't conflict.

 -- Thomas Goirand <zigo@debian.org>  Mon, 30 Mar 2015 10:31:06 +0200

python-json-pointer (1.0-2) unstable; urgency=low

  * Ran wrap-and-sort.
  * Adds support for python3.
  * Adds Ubuntu patch for utf-8 in setup.py fixing FTBFS.
  * Adds README.md in the doc folders.
  * Cleans correctly (allow to build twice).
  * Standards-Version: 3.9.4

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 May 2013 14:02:39 +0800

python-json-pointer (1.0-1) unstable; urgency=low

  * Uploading to unstable.
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 11 May 2013 06:56:05 +0000

python-json-pointer (0.6-2) experimental; urgency=low

  * Added patch from Ubuntu to run tests at build time (Closes: #702292).

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Mar 2013 05:27:55 +0000

python-json-pointer (0.6-1) experimental; urgency=low

  * Initial release (Closes: #699507).

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Oct 2012 11:55:15 +0000
